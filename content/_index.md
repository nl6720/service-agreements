---
type: docs
---

This page contains the Arch Linux legal documentation.

Git repository: https://gitlab.archlinux.org/archlinux/service-agreements
